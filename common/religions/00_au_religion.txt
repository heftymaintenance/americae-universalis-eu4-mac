# If you add religions, and use those tags, do not change them without changing everywhere they are used.

# Uses all 'modifiers' possible thats exported as a Modifier.

debugo = {

	flags_with_emblem_percentage = 0 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 420 #Testland

	testlandish = {
		color = { 198 182 212 }
		icon = 66
		country = {
			tolerance_heathen = 1
			adm_tech_cost_modifier = -0.1
		}
		country_as_secondary = {
			adm_tech_cost_modifier = -0.1
		}
		heretic = { TAOIST }
		uses_harmony = yes
	}
	
	placeholderite = {
		color = { 55 59 67 }
		icon = 41
		country = {
			tolerance_heathen = 6
		}
		country_as_secondary = {
			tolerance_heathen = 3
		}
		heretic = { CHAOS_WORSHIPPERS }
		personal_deity = yes
		
		harmonized_modifier = harmonized_placeholderite
	}

	crusade_name = CRUSADE
}