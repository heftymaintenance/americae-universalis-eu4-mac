# Do not change tags in here without changing every other reference to them.
# Do not change tags in here without changing every other reference to them.
# If adding new groups or ideas, make sure they are unique.


### Nation specific idea groups

#Placeholder and unbalanced

#Gotham ideas
A01_ideas = {
	start = {
		cavalry_power = 0.2
		diplomatic_upkeep = 1
	}

	bonus = {
		discipline = 0.05
	}
	
	trigger = {
		OR = {
			tag = A01
		}
	}
	free = yes		#will be added at load.
	
	A01_fun_friends = {
		vassal_forcelimit_bonus = 1.0
		reduced_liberty_desire = 5
	}
	A01_big_ole_apple = {
		prestige = 1
	}
	A01_punch_real_aggresivley = {
		land_morale = 0.15
	}
	A01_real_smart = {
		all_power_cost = 0.1
	}
	A01_fast_talkers_phun_phrases = {
		improve_relation_modifier = 0.10
		spy_offence = 0.1
	}
	A01_lets_get_old_york = {
		colonists = 1
	}
	A01_good_boats = {
		blockade_efficiency = 0.3
		capture_ship_chance = 0.5
	}
}