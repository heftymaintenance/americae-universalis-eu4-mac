# has_native_migration_government = {
	# OR = {
		# has_reform = adventurer_reform
		# government = native
	# }
# }

# has_native_migration_government_except_adventurer = {
	# OR = {
		# government = native
	# }
# }

# is_adventurer = {
	# OR = {
	
		# #America
		# tag = TBD
		
		# #Old World
		# tag = TBD

	# }
# }

# is_old_world_colonial_spawnable = {
	# tag = TBD
# }

# is_temple = {
	# OR = {
		# #Europe
		# tag = TBD #Placeholder
	# }
# }


province_with_farm_goods = {
	OR = {
		trade_goods = grain
		trade_goods = livestock
		trade_goods = wine
		trade_goods = spices
		trade_goods = tea
		trade_goods = coffee
		trade_goods = cocoa
		trade_goods = cotton
		trade_goods = sugar
		trade_goods = tobacco
		trade_goods = silk
	}
}


province_with_mineable_goods = {
	OR = {
		trade_goods = gold
		trade_goods = copper
		trade_goods = iron
		trade_goods = mithril
		trade_goods = gems
		trade_goods = salt
	}
}

province_with_urban_goods = {
	OR = {
		trade_goods = glass
		trade_goods = paper
		trade_goods = cloth
		trade_goods = chinaware
	}
}

ruler_has_max_personalities = {
	calc_true_if = {
		ROOT = {
				ruler_has_personality = just_personality
				ruler_has_personality = righteous_personality
				ruler_has_personality = tolerant_personality
				ruler_has_personality = kind_hearted_personality
				ruler_has_personality = free_thinker_personality
				ruler_has_personality = well_connected_personality
				ruler_has_personality = calm_personality
				ruler_has_personality = careful_personality
				ruler_has_personality = secretive_personality
				ruler_has_personality = intricate_web_weaver_personality
				ruler_has_personality = fertile_personality
				ruler_has_personality = well_advised_personality
				ruler_has_personality = benevolent_personality
				ruler_has_personality = zealot_personality 
				ruler_has_personality = pious_personality
				ruler_has_personality = lawgiver_personality
				ruler_has_personality = midas_touched_personality
				ruler_has_personality = incorruptible_personality
				ruler_has_personality = architectural_visionary_personality
				ruler_has_personality = scholar_personality
				ruler_has_personality = entrepreneur_personality
				ruler_has_personality = industrious_personality
				ruler_has_personality = expansionist_personality
				ruler_has_personality = charismatic_negotiator_personality
				ruler_has_personality = conqueror_personality
				ruler_has_personality = silver_tongue_personality
				ruler_has_personality = tactical_genius_personality
				ruler_has_personality = bold_fighter_personality
				ruler_has_personality = strict_personality
				ruler_has_personality = inspiring_leader_personality
				ruler_has_personality = martial_educator_personality
				ruler_has_personality = navigator_personality
				ruler_has_personality = fierce_negotiator_personality
				ruler_has_personality = babbling_buffoon_personality
				ruler_has_personality = embezzler_personality
				ruler_has_personality = infertile_personality
				ruler_has_personality = drunkard_personality
				ruler_has_personality = sinner_personality
				ruler_has_personality = greedy_personality
				ruler_has_personality = cruel_personality
				ruler_has_personality = craven_personality
				ruler_has_personality = naive_personality
				ruler_has_personality = loose_lips_personality
				ruler_has_personality = obsessive_perfectionist_personality
				ruler_has_personality = malevolent_personality
				ruler_has_personality = immortal_personality   #We count it
		}
	
		amount = 3
	}
}

#Checks if $tag$ has lower mil tech than the country scope the scripted trigger is within
mil_tech_is_inferior = {
	variable_arithmetic_trigger = {
		export_to_variable = {
			which = root_tech
			value = mil_tech
		}
		export_to_variable = {
			which = from_tech
			value = mil_tech
			who = $tag$
		}
		
		subtract_variable = { root_tech = 1 }
		
		check_variable = {
			which = root_tech
			which = from_tech
		}
	}
}

#Checks if $tag$ has lower dip tech than the country scope the scripted trigger is within
dip_tech_is_inferior = {
	variable_arithmetic_trigger = {
		export_to_variable = {
			which = root_tech
			value = dip_tech
		}
		export_to_variable = {
			which = from_tech
			value = dip_tech
			who = $tag$
		}
		
		subtract_variable = { root_tech = 1 }
		
		check_variable = {
			which = root_tech
			which = from_tech
		}
	}
}

#Checks if $tag$ has lower adm tech than the country scope the scripted trigger is within
adm_tech_is_inferior = {
	variable_arithmetic_trigger = {
		export_to_variable = {
			which = root_tech
			value = adm_tech
		}
		export_to_variable = {
			which = from_tech
			value = adm_tech
			who = $tag$
		}
		
		subtract_variable = { root_tech = 1 }
		
		check_variable = {
			which = root_tech
			which = from_tech
		}
	}
}


has_fort_building_trigger = {
	OR = {
		has_building = fort_15th
		has_building = fort_16th
		has_building = fort_17th
		has_building = fort_18th
	}
}

hre_elector_majority = {
	if = {
		limit = { num_of_electors = 7 }
		calc_true_if = {
			all_elector = { preferred_emperor = $tag$ }
			amount = 4
		}
	}
	else_if = {
		limit = { num_of_electors = 5 }
		calc_true_if = {
			all_elector = { preferred_emperor = $tag$ }
			amount = 3
		}
	}
	else_if = {
		limit = { num_of_electors = 3 }
		calc_true_if = {
			all_elector = { preferred_emperor = $tag$ }
			amount = 2
		}
	}
	else_if = {
		limit = { num_of_electors = 1 }
		calc_true_if = {
			all_elector = { preferred_emperor = $tag$ }
			amount = 1
		}
	}
}

has_legitimacy_equivalent = {
	if = {
		limit = { government = monarchy }
		legitimacy = $VAL$
	}
	else_if = {
		limit = { government = republic }
		republican_tradition = $VAL$
	}
	else_if = {
		limit = { government = theocracy }
		if = {
			limit = { has_dlc = "Common Sense" }
			devotion = $VAL$
		}
		else = {
			custom_trigger_tooltip = {
				tooltip = automatic_bypass_tooltip
				always = yes
			}
		}
	}
	else_if = {
		limit = { government = tribal }
		if = {
			limit = {
				has_dlc = "The Cossacks"
				is_nomad = yes
			}
			horde_unity = $VAL$
		}
		else = { legitimacy = $VAL$ }
	}
}

has_professionalism_equivalent = {
	if = {
		limit = { has_dlc = "Cradle of Civilization" }
		army_professionalism = $VAL$
	}
	else = {
		custom_trigger_tooltip = {
			tooltip = automatic_bypass_tooltip
			always = yes
		}
	}
}

#hcc_is_dismantled = {
#	custom_trigger_tooltip = {
#		tooltip = hcc_dismantled_tooltip
#		NOT = {
#			hre_size = 1
#			exists = HCC #Holy Columbian Confederacy
#		}
#	}
#}

has_coastal_defence_building = {
	OR = {
		has_building = coastal_defence
		has_building = naval_battery
	}
}

has_any_building_trigger = {
	OR = {
		has_tax_building_trigger = yes
		has_coastal_defence_building = yes
		has_fort_building_trigger = yes
		has_manpower_building_trigger = yes
		has_production_building_trigger = yes
		has_dock_building_trigger = yes
		has_trade_building_trigger = yes
		has_shipyard_building_trigger = yes
		has_forcelimit_building_trigger = yes
		has_dock_building_trigger = yes
		has_manufactory_trigger = yes
		has_courthouse_building_trigger = yes
		has_building = university
	}
}

heir_has_max_personalities = {
	calc_true_if = {
		ROOT = {
				heir_has_personality = just_personality
				heir_has_personality = righteous_personality
				heir_has_personality = tolerant_personality
				heir_has_personality = kind_hearted_personality
				heir_has_personality = free_thinker_personality
				heir_has_personality = well_connected_personality
				heir_has_personality = calm_personality
				heir_has_personality = careful_personality
				heir_has_personality = secretive_personality
				heir_has_personality = intricate_web_weaver_personality
				heir_has_personality = fertile_personality
				heir_has_personality = well_advised_personality
				heir_has_personality = benevolent_personality
				heir_has_personality = zealot_personality 
				heir_has_personality = pious_personality
				heir_has_personality = lawgiver_personality
				heir_has_personality = midas_touched_personality
				heir_has_personality = incorruptible_personality
				heir_has_personality = architectural_visionary_personality
				heir_has_personality = scholar_personality
				heir_has_personality = entrepreneur_personality
				heir_has_personality = industrious_personality
				heir_has_personality = expansionist_personality
				heir_has_personality = charismatic_negotiator_personality
				heir_has_personality = conqueror_personality
				heir_has_personality = silver_tongue_personality
				heir_has_personality = tactical_genius_personality
				heir_has_personality = bold_fighter_personality
				heir_has_personality = strict_personality
				heir_has_personality = inspiring_leader_personality
				heir_has_personality = martial_educator_personality
				heir_has_personality = navigator_personality
				heir_has_personality = fierce_negotiator_personality
				heir_has_personality = babbling_buffoon_personality
				heir_has_personality = embezzler_personality
				heir_has_personality = infertile_personality
				heir_has_personality = drunkard_personality
				heir_has_personality = sinner_personality
				heir_has_personality = greedy_personality
				heir_has_personality = cruel_personality
				heir_has_personality = craven_personality
				heir_has_personality = naive_personality
				heir_has_personality = loose_lips_personality
				heir_has_personality = obsessive_perfectionist_personality
				heir_has_personality = malevolent_personality
				heir_has_personality = immortal_personality   #We count it
		}
	
		amount = 3
	}
}

consort_has_max_personalities = {
	calc_true_if = {
		ROOT = {
				consort_has_personality = just_personality
				consort_has_personality = righteous_personality
				consort_has_personality = tolerant_personality
				consort_has_personality = kind_hearted_personality
				consort_has_personality = free_thinker_personality
				consort_has_personality = well_connected_personality
				consort_has_personality = calm_personality
				consort_has_personality = careful_personality
				consort_has_personality = secretive_personality
				consort_has_personality = intricate_web_weaver_personality
				consort_has_personality = fertile_personality
				consort_has_personality = well_advised_personality
				consort_has_personality = benevolent_personality
				consort_has_personality = zealot_personality 
				consort_has_personality = pious_personality
				consort_has_personality = lawgiver_personality
				consort_has_personality = midas_touched_personality
				consort_has_personality = incorruptible_personality
				consort_has_personality = architectural_visionary_personality
				consort_has_personality = scholar_personality
				consort_has_personality = entrepreneur_personality
				consort_has_personality = industrious_personality
				consort_has_personality = expansionist_personality
				consort_has_personality = charismatic_negotiator_personality
				consort_has_personality = conqueror_personality
				consort_has_personality = silver_tongue_personality
				consort_has_personality = tactical_genius_personality
				consort_has_personality = bold_fighter_personality
				consort_has_personality = strict_personality
				consort_has_personality = inspiring_leader_personality
				consort_has_personality = martial_educator_personality
				consort_has_personality = navigator_personality
				consort_has_personality = fierce_negotiator_personality
				consort_has_personality = babbling_buffoon_personality
				consort_has_personality = embezzler_personality
				consort_has_personality = infertile_personality
				consort_has_personality = drunkard_personality
				consort_has_personality = sinner_personality
				consort_has_personality = greedy_personality
				consort_has_personality = cruel_personality
				consort_has_personality = craven_personality
				consort_has_personality = naive_personality
				consort_has_personality = loose_lips_personality
				consort_has_personality = obsessive_perfectionist_personality
				consort_has_personality = malevolent_personality
				consort_has_personality = immortal_personality   #We count it
		}
	
		amount = 3
	}
}

# has_crusader_religion = {
	# OR = {
		# religion = ursuline
		# religion = placeholder
	# }
# }

has_adm_advisor_4 = {
	custom_trigger_tooltip = {
		tooltip = has_adm_advisor_4_tooltip
		OR = {
			treasurer = 4
			philosopher = 4
			artist = 4
			theologian = 4
			master_of_mint = 4
			inquisitor = 4
			natural_scientist = 4
		}
	}
}

has_adm_advisor_5 = {
	custom_trigger_tooltip = {
		tooltip = has_adm_advisor_5_tooltip
		OR = {
			treasurer = 5
			philosopher = 5
			artist = 5
			theologian = 5
			master_of_mint = 5
			inquisitor = 5
			natural_scientist = 5
		}
	}
}

has_dip_advisor_4 = {
	custom_trigger_tooltip = {
		tooltip = has_dip_advisor_4_tooltip
		OR = {
			statesman = 4
			diplomat = 4
			naval_reformer = 4
			trader = 4
			colonial_governor = 4
			navigator = 4
			spymaster = 4
		}
	}
}

has_dip_advisor_5 = {
	custom_trigger_tooltip = {
		tooltip = has_dip_advisor_5_tooltip
		OR = {
			statesman = 5
			diplomat = 5
			naval_reformer = 5
			trader = 5
			colonial_governor = 5
			navigator = 5
			spymaster = 5
		}
	}
}

has_mil_advisor_4 = {
	custom_trigger_tooltip = {
		tooltip = has_mil_advisor_4_tooltip
		OR = {
			army_organiser = 4
			army_reformer = 4
			commandant = 4
			grand_captain = 4
			recruitmaster = 4
			fortification_expert = 4
			quartermaster = 4
		}
	}
}

has_mil_advisor_5 = {
	custom_trigger_tooltip = {
		tooltip = has_mil_advisor_5_tooltip
		OR = {
			army_organiser = 5
			army_reformer = 5
			commandant = 5
			grand_captain = 5
			recruitmaster = 5
			fortification_expert = 5
			quartermaster = 5
		}
	}
}
