# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Triggered modifiers are here.
# these are checked for each countries once/month and then applied.
#
# Effects are fully scriptable here.


###########################################
# HCC dominant religion bonus
###########################################


#Balancer for Crown Land buffs if you're a faction country with estates

crown_land_faction_balancer_100 = {
	potential = { 
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 100
	}

	trigger = {
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 100
	}
	global_tax_modifier = -0.2
	global_autonomy = 0.05
	yearly_absolutism = -1
	max_absolutism = -15
}

crown_land_faction_balancer_75_99 = {
	potential = { 
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 75
		NOT = { crown_land_share = 100 }
	}

	trigger = {
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 75
		NOT = { crown_land_share = 100 }
	}
	global_tax_modifier = -0.15
	global_autonomy = 0.01
	yearly_absolutism = -1
	max_absolutism = -15
}

crown_land_faction_balancer_60_74 = {
	potential = { 
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 60
		NOT = { crown_land_share = 74 }
	}

	trigger = {
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 60
		NOT = { crown_land_share = 74 }
	}
	global_tax_modifier = -0.1
	max_absolutism = -10
	yearly_absolutism = -1
}

crown_land_faction_balancer_50_59 = {
	potential = { 
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 50
		NOT = { crown_land_share = 59 }
	}

	trigger = {
		has_factions = yes
		has_any_estates = yes
		crown_land_share = 50
		NOT = { crown_land_share = 59 }
	}
	global_tax_modifier = -0.05
	max_absolutism = -5
}
