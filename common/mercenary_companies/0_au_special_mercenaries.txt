#This file is for special Mercs. Such as thorugh Missions or Decisions.

#Special: Fanciest Place Holders (For the Fancy Shmacny Placelords)
merc_aldresian_knights = {
    regiments_per_development = 0.01
	cavalry_weight = 0.4
	cavalry_cap = 6
	home_province = 420
	sprites = { westerngfx_sprite_pack }
    trigger = {
		has_country_modifier = place_based_mercs
		OR = {
			is_emperor = yes
			tag = Z99 #The Place-Testland Union
		}
	}
	cost_modifier = 1.5
	modifier = {
		shock_damage_received = -0.2
		land_morale = 0.2
		discipline = 0.1
	}
}