# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 28 31 150 }



historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	quantity_ideas
	religious_ideas
	expansion_ideas
	quality_ideas	
	innovativeness_ideas
	offensive_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = { #Rebalacne the percent of names, right now its alphabetical.
	"Giorge #0" = 100
	"Adriaen #0" = 30
	"Abrahan #0" = 30
	"Andrew #0" = 30
	"Aaron #0" = 30
	"Ardolph #0" = 30
	"Achilles #0" = 30
	"Agapett #0" = 30
	"Alberht #0" = 30
	"Alessander #0" = 30
	"Arnold #0" = 30
	"Aurel #0" = 30
	"Bartolomeus #0" = 30
	"Basil #0" = 30
	"Benicio #0" = 30
	"Beppe #0" = 30
	"Berthold #0" = 30
	"Bertrand #0" = 30
	"Brennan #0" = 30
	"Bruce #0" = 30
	"Bruno #0" = 30
	"Boniface #0" = 30
	"Caesar #0" = 30
	"Christopher #0" = 30
	"Cornelis #0" = 30
	"Corvo #0" = 30
	
	"Caleb #0" = 30
	"Carles #0" = 30
	"Cadwallader #0" = 30
	"Casimir #0" = 30
	"Corydon #0" = 30
	"David #0" = 30
	"Daniel #0" = 30
	"Dante #0" = 30
	"Dorian #0" = 30
	"Ebenezer #0" = 30
	"Edward #0" = 30
	"Eastman #0" = 30
	"Elmo #0" = 30
	"Emeric #0" = 30
	"Erasmis #0" = 30
	"Ercole #0" = 30
	"Eusebe #0" = 30
	"Eustace #0" = 30
	"Eugeny #0" = 30
	"Ezio #0" = 30
	"Faris #0" = 30
	
	"Francelin #0" = 20
	"Florell #0" = 20
	"Frederic #0" = 20
	"Fernand #0" = 20
	"Ferrutt #0" = 20
	"Flavis #0" = 20
	"Francis #0" = 20
	"Giovannes #0" = 20
	"Gerard #0" = 20
	"Gennard #0" = 20
	"Gianluke #0" = 20
	"Gianpaul #0" = 20
	"Gioacchim #0" = 20
	"Girald #0" = 20
	"Grigory #0" = 20
	"Hari #0" = 20
	"Hayan #0" = 20
	"Henreyk #0" = 20
	"Hector #0" = 20
	"Horatis #0" = 20
	"Humbert #0" = 20
	"Idrees #0" = 20
	"Ignatius #0" = 20
	"Isaac #0" = 20
	"Isaiah #0" = 20
	"Isidorr #0" = 20
	"Jason #0" = 20
	
	"Jonas #0" = 20
	"Jacobbe #0" = 30
	"Joaquin #0" = 30
	"Julius #0" = 30
	"Jaime #0" = 30
	"Kiran #0" = 30
	"Koch #0" = 30
	"Lambert #0" = 30
	"Lewis #0" = 30
	"Levi #0" = 30
	"Ludovick #0" = 30
	"Leander #0" = 30
	"Leonard #0" = 30
	"Leone #0" = 30
	"Loren #0" = 30
	"Lucis #0" = 30
	"Luis #0" = 30
	"Marius #0" = 30
	"Manohar #0" = 30
	"Meyer #0" = 30
	"Michael #0" = 30
	"Marcus #0" = 30
	"Mattheus #0" = 30
	"Mohan #0" = 30
	"Nicholas #0" = 30
	"Narciss #0" = 30
	"Nevis #0" = 30
	"Nestor #0" = 30
	"Nunzis #0" = 30
	"Oldstead #0" = 30
	"Oswald #0" = 30
	
	"Ottavis #0" = 30
	"Orion #0" = 30
	"Ottone #0" = 30
	"Param #0" = 30
	"Pieter #0" = 30
	"Paulus #0" = 30
	"Peregrin #0" = 30
	"Patricis #0" = 30
	"Philippe #0" = 30
	"Rama #0" = 30
	"Rudy #0" = 30
	"Richard #0" = 30
	"Robertus #0" = 30
	"Ryan #0" = 30
	"Sameer #0" = 30
	"Samuel #0" = 30
	"Stephen #0" = 30
	"Seth #0" = 30
	"Stratis #0" = 30
	"Salvator #0" = 30
	"Sylvester #0" = 30
	"Silvis #0" = 30
	"Sedgewick #0" = 30
	
	"Theo #0" = 30
	"Thom #0" = 30
	"Thaddeus #0" = 30
	"Tacit #0" = 30
	"Tullis #0" = 30
	"Ulderic #0" = 30
	"Varian #0" = 30
	"Vasily #0" = 30
	"Venceslaus #0" = 30
	"Vinay #0" = 30
	"Vincent #0" = 30
	"Victor_ #0" = 30
	"Wualter #0" = 30
	"Wuillem #0" = 30
	"Wagner #0" = 30
	"York #0" = 30
	"Yohan #0" = 30
	"Zachari #0" = 30
	
	"Ada #0" = -10
	"Albertine #0" = -10
	"Alda #0" = -10
	"Alita #0" = -10
	"Amalia #0" = -10
	"Amina #0" = -10
	"Anna #0" = -10
	"Annalise #0" = -10
	"Annetta #0" = -10
	"Anita #0" = -10
	"Anastacey #0" = -10
	"Alexia #0" = -10
	"Arya #0" = -10
	"Barbara #0" = -10
	"Berenice #0" = -10
	"Bernardina #0" = -10
	"Bridget #0" = -10
	"Brunella #0" = -10
	"Carmen #0" = -10
	"Catharyna #0" = -10
	"Chelsea #0" = -10
	"Caro #0" = -10
	"Clarice #0" = -10
	"Chelo #0" = -10
	
	"Christina #0" = -10
	"Carlotte #0" = -10
	"Cynthia #0" = -10
	"Daisy #0" = -10
	"Dolores #0" = -10
	"Esmerald #0" = -10
	"Eleanor #0" = -10
	"Eunice #0" = -10
	"Estella #0" = -10
	"Emma #0" = -10
	"Fay #0" = -10
	"Francisca #0" = -10
	"Fiorina #0" = -10
	"Fioretta #0" = -10
	"Gemma #0" = -10
	"Gita #0" = -10
	"Graziella #0" = -10
	"Hermanna #0" = -10
	"Henrietta #0" = -10
	"Helen #0" = -10
	"Hana #0" = -10
	"Ines #0" = -10
	"Irene #0" = -10
	"Jordan #0" = -10
	"Juliana #0" = -10
	"Karam #0" = -10
	"Keiran #0" = -10
	"Lana #0" = -10
	"Lauren #0" = -10
	"Lila #0" = -10
	"Lola #0" = -10
	"Martha #0" = -10
	"Maria #0" = -10
	"Maya #0" = -10
	"Minnie #0" = -10
	
	"Margarita #0" = -10
	"Maxima #0" = -10
	"Magdalene #0" = -10
	"Mercedes #0" = -10
	"Miran #0" = -10
	"Misha #0" = -10
	"Mona #0" = -10
	"Myrtle #0" = -10
	"Nadia #0" = -10
	"Nari #0" = -10
	"Niki #0" = -10
	"Nina #0" = -10
	"Paris #0" = -10
	"Pauline #0" = -10
	"Pamela #0" = -10
	"Ramona #0" = -10
	"Rebecca #0" = -10
	"Rosalia #0" = -10
	"Rita #0" = -10
	"Regina #0" = -10
	
	"Renata #0" = -10
	"Rhoda #0" = -10
	"Sasha #0" = -10
	"Salome #0" = -10
	"Sanya #0" = -10
	"Stephanie #0" = -10
	"Sophia #0" = -10
	"Sori #0" = -10
	"Tania #0" = -10
	"Tara #0" = -10
	"Teresita #0" = -10
	"Thea #0" = -10
	"Vera #0" = -10
	"Victoria #0" = -10
	"Velma #0" = -10
	"Yolanda #0" = -10
	"Zelda #0" = -10
	"Zelina #0" = -10
	"Zenobia #0" = -10
}

leader_names = {
	#Country Specific
	Hasan Wesker Isley Rotsa Mercuccio Nassau Kefeller Attano "di Settignano" "of Manhattan" "of Brooklyn" "of Queens" "of the Bronx" "of Staten Island"
	
	#Province Neighbours
	"of Philadelphia" "of Ardmore" "of Baltimore" #Like that, might add more later
	
	#Country Neighbours
	"of Columbia" "of Keystone" "of Hudsonia" #Like that, might add more later
	
	#Geographic Neighbours
	"of New England" "of the Beltway" "of the Hudson" "of the Delaware" "of the Burroughs" "of the Shore"
	
	#Noble Families
	"van Wayne" "van Nigma" "van Stuyvesant" "van Cobblepot" "van Theovelt" "van Skulski" "van DellaRocca" "van Ger" "van Chen" "van Longacre" "van Laggard" "De Witt" "van Espinal" "van Falcone" Rothschild Gutfreund
	
 	#Generic Midatlantic
	Placeholder #Will compile namelists later
}

ship_names = {

	#Generic Midatlantic
	Placeholder #Will compile namelists later
	
	#American
	Placeholder #Will compile namelists later
	
	#Americanst
	Placeholder #Will compile namelists later
	
	#Gotham-specific
	"Place Holder" #Will compile namelists later

}

army_names = {
	Placeholder "Army of $PROVINCE$" #Will compile namelists later
}

fleet_names = {
	"Place Holder" #Will compile namelists later
}